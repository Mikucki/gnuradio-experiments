/* -*- c++ -*- */

#define KEELOQ_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "keeloq_swig_doc.i"

%{
#include "keeloq/dummy.h"
#include "keeloq/clock_recovery_zero_ff.h"
%}


%include "keeloq/dummy.h"
GR_SWIG_BLOCK_MAGIC2(keeloq, dummy);
%include "keeloq/clock_recovery_zero_ff.h"
GR_SWIG_BLOCK_MAGIC2(keeloq, clock_recovery_zero_ff);
