INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_KEELOQ keeloq)

FIND_PATH(
    KEELOQ_INCLUDE_DIRS
    NAMES keeloq/api.h
    HINTS $ENV{KEELOQ_DIR}/include
        ${PC_KEELOQ_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    KEELOQ_LIBRARIES
    NAMES gnuradio-keeloq
    HINTS $ENV{KEELOQ_DIR}/lib
        ${PC_KEELOQ_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(KEELOQ DEFAULT_MSG KEELOQ_LIBRARIES KEELOQ_INCLUDE_DIRS)
MARK_AS_ADVANCED(KEELOQ_LIBRARIES KEELOQ_INCLUDE_DIRS)

