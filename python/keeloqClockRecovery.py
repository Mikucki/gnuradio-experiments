#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
from gnuradio import gr

class keeloqClockRecovery(gr.basic_block):
    """
    docstring for block keeloqClockRecovery
    """
    
    sampleForNrOfSymbols = 0
    sampleForNrOfSymbolsMax = 10
    samplesPerSymbol = 95 /8
    samplesPerSymbolHalf = samplesPerSymbol / 2
    signalPositive = 0
    samplesTillSymbolMidpoint = 0
    
    
    def __init__(self):
        gr.basic_block.__init__(self,
            name="keeloqClockRecovery",
            in_sig=[numpy.float32],
            out_sig=[numpy.float32])

    def forecast(self, noutput_items, ninput_items_required):
        #setup size of input_items[i] for work call
        for i in range(len(ninput_items_required)):
            ninput_items_required[i] = noutput_items

    def general_work(self, input_items, output_items):
        #output_items[0][:] = input_items[0]
        samplesLeft = len(input_items[0])
        if samplesLeft < 0:
            return 0
        currentOutputSample = 0
        
        if self.signalPositive == 0: # didn't sample after edge
            #print "Found signal sign change"
            if input_items[0][0] >= 0:
                self.signalPositive = 1 #positive
            else:
                self.signalPositive = -1 #negative
                    
        while samplesLeft > 0 and currentOutputSample < len(output_items[0]):
            signalValue = input_items[0][len(input_items[0])-samplesLeft]
            
            if signalValue > 0:
                signalValueUnity = 1
            else:
                signalValueUnity = -1
                    
            if signalValueUnity != self.signalPositive:
                #print "Found sign change"
                self.sampleForNrOfSymbols = self.sampleForNrOfSymbolsMax
                self.samplesTillSymbolMidpoint = self.samplesPerSymbolHalf 
                self.signalPositive = signalValueUnity
                
            if self.sampleForNrOfSymbols > 0:             
                if self.samplesTillSymbolMidpoint == 0:
                    
                    self.sampleForNrOfSymbols -= 1
                    
                    if signalValue > 0:
                        output_items[0][currentOutputSample] = 1
                    else:
                        output_items[0][currentOutputSample] = -1
                    #print "Stored", output_items[0][currentOutputSample]
                    self.samplesTillSymbolMidpoint = self.samplesPerSymbol
                    currentOutputSample += 1
                    samplesLeft -= 1
                    break
                    
            samplesLeft -= 1
            
            if self.samplesTillSymbolMidpoint > 0:
                self.samplesTillSymbolMidpoint -= 1
            
                    
        #print "Processed", len(input_items[0])-samplesLeft, "samples"
                    
        self.consume(0, len(input_items[0])-samplesLeft)
        #print "Consumed", len(input_items[0])-samplesLeft
        #self.consume_each(len(input_items[0]))
        return currentOutputSample
